import { cyan } from "color-name"

describe('Search storeID', function() {
    it('test search storeID', function() {
       
        cy.visit('https://manage.wefresh.me/wefresh-admin')
        cy.xpath('//input[@title="Username"]').type('admin')
        cy.xpath('//input[@title="Password"]').type('senditadmin')
        cy.get('.sc-hSdWYo').click('')
        cy.xpath('//span[text()="Store"]').click('')
        cy.get(':nth-child(1) > a').click('')
        //storetab.map(value => cy.xpath(`${value}`).click())
        var storeID =['L952062','L952064','L952065','L952066','L952067','L952068','L952027',
        'L952069',
        'L952070',
        'L952071',
        'L952085',
        'L952088',
        'L952090',
        'L952091',
        'L952092',
        'L952093',
        'L952094',
        'L952095',
        'L952097',
        'L952098',
        'L952101',
        'L952102',
        'L952103',
        'L952104',
        'L952105',
        'L952106',
        'L952107',
        'L952108',
        'L952146',
        'L952147',
        'L952149',
        'L952150',
        'L952151',
        'L952153',
        'L952154',
        'L952156',
        'L952157',
        'L952318',
        'L952321',
        'L952322',
        'L952171',
        'L952172',
        'L952173',
        'L952174',
        'L952175',
        'L952176',
        'L952178',
        'L952179',
        'L952180',
        'L952181',
        'L952204',
        'L952205',
        'L952206',
        'L952208',
        'L952209',
        'L952210',
        'L952211',
        'L952212',
        'L952213',
        'L952214',
        'L952215',
        'L952216',
        'L952217',
        'L952218', 'L952220','L952222','L949211',]
        //const waitload = [cy.get('.ant-spin-dot').should('not.be.visible')]
        const waitload = () =>
        cy.get('.ant-spin-dot')
        .should('not.be.visible')
        var x 
        Cypress.Commands.add(
            'shouldHaveTrimmedText',
            {
                prevSubject: true,
            },
            (subject, equalTo) => {
                if (isNaN(equalTo)) {
                    expect(subject.text()).to.eq(equalTo);
                } else {
                    expect(parseInt(subject.text())).to.eq(equalTo);
                }
                return subject;
            },
        );
        for(x in storeID){
        
        cy.get('#storeID').clear('').type(`${storeID[x]}`) 
        cy.get('.ant-form-item-children > .ant-btn').click('')
    
        waitload()
        //cy.waitUntil(() => cy.get('.ant-spin-dot').should('not.be.visible'));
       // cy.get('[data-cy=Syncing Complete]').shouldHaveTrimmedText('Syncing Complete')
        cy.xpath('//td[@class]/p[text()="Syncing Complete"]').shouldHaveTrimmedText('Syncing Complete')
        }
        
        
    })
  })
  describe('Search productID', function() {
    it('test search productId', function() {
        cy.visit('https://manage.wefresh.me/wefresh-admin')
        cy.xpath('//input[@title="Username"]').type('admin')
        cy.xpath('//input[@title="Password"]').type('senditadmin')
        cy.get('.sc-hSdWYo').click('')
    
        cy.get(':nth-child(1) > .ant-menu-submenu-title > :nth-child(1) > .sc-gZMcBi').click('')
        cy.get(':nth-child(1) > a').click('')
        const waitload = () =>
        cy.get('.ant-spin-dot')
        .should('not.be.visible')
        const productId = ['L91636234' , 'L91636239' , 'L91636249' , 'L91636236' 
        , 'L91636251' , 'L91636256' , 'L91636272' , 'L91636258' , 'L91636274']
        
        var x
        
        for(x in productId){
            cy.get('#id').clear('').type(`${productId[x]}`)  
            cy.get('.ant-form-item-children > .ant-btn').click('')
            waitload()
            cy.xpath(`//td[text()="${productId[x]}"]`).should('not.be.visible')
        }
            
    })
})