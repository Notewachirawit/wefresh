

describe('Create category', function() {
    it('Create root category', function() {
        
        
        cy.visit('http://admin-dev.wefresh.me/auth/login')
        cy.viewport(1920, 1080)
        cy.get('[placeholder="Email"]').type('test@hotmail.com')
        cy.get('[placeholder="Password"]').type('note1234')
        cy.get('[type="submit"]').click()
        cy.visit('http://admin-dev.wefresh.me/dashboard/categories')
        //add root category 
        const textfieldcategory = ['//input[@id="category_root_fields_form_name_th"]','//input[@id="category_root_fields_form_name_en"]','//textarea[@id="category_root_fields_form_description_th"]','//textarea[@id="category_root_fields_form_description_en"]']
        const categoriesInfo = ['Featured Products', 'Featured Products', 'Featured Products', 'Featured Products']
        cy.get('[href="/dashboard/categories/create/root"]').click('')
        
        textfieldcategory.map((value, index) => cy.xpath(value).type(categoriesInfo[index]))
        cy.xpath('//button[@type="submit"]').click('')
    })
})

describe('Create product', function() {
    it('Login wefresh admin ', function() {
        
        
        cy.visit('http://admin-dev.wefresh.me/auth/login')
        cy.viewport(1920, 1080)
        
        cy.get('[placeholder="Email"]').type('test@hotmail.com')
        cy.get('[placeholder="Password"]').type('note1234{enter}')
        //cy.get('[type="submit"]').click()
   
   
   
        cy.visit('http://admin-dev.wefresh.me/dashboard/products/create')
        //General Information
        const textfieldinformation = ['product_info_name_en','product_info_name_th','product_info_brand','product_info_description_th','product_info_description_en']
        const categoriesInfo = ['Excellent Streetfood', 'อาหารตามสั่ง ', 'Excellent Streetfood ','อาหารตามสั่ง','Excellent Streetfood ']
        textfieldinformation.map((value, index) => cy.xpath(`//*[@id="${value}"]`).type(categoriesInfo[index]))
        //cy.xpath('//label[text()="Weight"]/../../div/div//input[@role="spinbutton"]').clear('').type('200')
        cy.xpath('//span[@class="ant-select-search__field__placeholder"]').click('')
        cy.xpath('//span[text()="Side dish"]').click({ force: true })
        const dropEvent = {
            dataTransfer: {
                
                files: [
                ],
            },
        };
        const upload= ['DSC_0135.jpg']
        const uploadbutton= ['.ant-upload > .ant-btn']//,'.ant-upload-picture-card-wrapper > .ant-upload-select > .ant-upload']
        
        // upload.map(value =>{ cy.fixture(`${value}`).then((picture) => {
        //     return Cypress.Blob.base64StringToBlob(picture, 'image/png').then((blob) => {
        //         console.log(blob)
        //         dropEvent.dataTransfer.files.push(blob);
        //     });
        // });
        // check api for uploaded function
        cy.server()
        cy.route('POST', 'development/*').as('upload-files')
        upload.map(pictureName => {
          cy.fixture(pictureName).then(picture => {
            Cypress.Blob.base64StringToBlob(picture, 'image/png').then(blob => {
              dropEvent.dataTransfer.files[0] = blob
                uploadbutton.map(value => cy.get(`${value}`).trigger('drop', dropEvent))
              console.log(dropEvent)
              console.log(uploadbutton)
            })
          })
        })
   
        
        cy.get('[title="Remove file"]').should('be.visible')
        cy.wait('@upload-files').its('status').should('eq', 200)
        cy.get('.ant-upload-list-item-image').should('have.class','ant-upload-list-item-image')
        cy.get('.ant-btn-primary').click('')
     

       
       
    
        //Pricing tax//

        const price = ['69']
        cy.xpath('//label[text()="Retail price (before tax)"]/../..//div/input[@role="spinbutton"]').clear('').type(`${price}`)
        cy.get('.ant-btn-primary').click('')
        cy.xpath('//button[text()="Finish"]').click('')

      }) 
})