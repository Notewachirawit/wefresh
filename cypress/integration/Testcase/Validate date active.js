

// describe('Create product', function() {
//     it('Login wefresh admin ', function() {
        
//       const isInCypress = window.Cypress ? true : false;
//       // if app is running by Cypress, then uses the liff mock object
//       if (isInCypress) {
//         window.liff = window.Cypress.liffMock;
//       }
//         cy.visit('https://admin-dev.wefresh.me/dashboard/products')
//         cy.viewport(1920, 1080)
//         window.Cypress.liffMock = {
//           init: cy.stub().as('init').resolves(),
//           isLoggedIn: cy.stub().as('challenge').returns(true),
//           // getProfile: cy.stub().as('getProfile').resolves(profile),
//           // isInClient: cy.stub().as('isInClient').returns(true),
//           // getOS: cy.stub().as('getOS').returns('web'),
//           // getLanguage: cy.stub().as('getLanguage').returns(navigator.language),
//           // getVersion: cy.stub().as('getVersion').returns('9999'),
//           // getFriendship: cy.stub().as('getFriendship').resolves({
//           //   friendFlag: true
//           // }),
//         }
//         // cy.get('[placeholder="Email"]').type('test@hotmail.com')
//         // cy.get('[placeholder="Password"]').type('note1234{enter}')
//         cy.wait(5000)
//         cy.get(':nth-child(1) > a').click('')
//         for (i = 1; i < 11; i++){
//         cy.get('.ant-select-selection-selected-value',{timeout:15000}).click('')
//         cy.get('[role="option"]').siblings()
//          cy.xpath('//li[text()="wholesale"]').scrollIntoView().click('')
//          var i
        

//           cy.xpath(`//tr[${i}]//td//span/button[@type="button"]/i`).click({ force: true })
//           cy.xpath('//li[text()="Edit"]').click({ force: true })
//           cy.xpath('//span[@class="ant-select-search__field__placeholder"]').click('')
//           cy.xpath('//li/span/span[text()="Featured product"]').click({ force: true })
//           cy.get('.ant-btn-primary').click('')
//           cy.xpath('//button[@class="ant-btn ant-popover-open ant-btn-cancel"]').click('')
//           cy.xpath('//button/span[text()="Yes"]').click({ force: true })
//          }
         
//         //cy.get('[type="submit"]').click()
//         //console.log(logelement)
//         //cy.wrap({title:'seven'}).its('title').should('eq','seven') //verify merchant when user selected 
      
        

//       }) 
// })
describe('todos API', () => {
  it('navigates', () => {
    cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
    cy.viewport(1920, 1080)
    cy.get('[class="product-list-title"]',{timeout:5000})
   
    cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
    cy.xpath('//input[@type="number"]').clear('').type('2')
    cy.get('[class="clect"]').scrollIntoView().click('')
    cy.xpath('//li[text()="เขียว"]').click('')
    cy.get('[data-test="add-to-cart"]').click('')
    cy.get('[class="btn btn-ghost"]').click('')
    
    // verify product price
     cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
     // verify product detail 
     cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
    // verify quantity product
    cy.get('[type="number"]').should('have.value','2')

  })
  
  // split visiting different origin in another test
  it('navigates to new origin', () => {
    cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
    
    cy.get('[class="product-list-title"]',{timeout:2000})
    cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
    
    var x
    var quantity = ['0.5','1,5','100']
    for(x in quantity){
        cy.xpath('//input[@type="number"]').clear('').type(`${quantity}`)
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click('')

    }
  })
})
