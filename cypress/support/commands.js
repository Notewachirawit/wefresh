// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-file-upload';
import 'cypress-wait-until';
Cypress.Commands.add('uploadFile', (fixtureFileName, inputSelector, mimeType = '') => {
    return cy.get(inputSelector).then(subject => {
        return cy.fixture(fixtureFileName, 'base64')
            .then(Cypress.Blob.base64StringToBlob)
            .then(blob => {
                const el = subject[0];
                const nameSegments = fixtureFileName.split('/');
                const name = nameSegments[nameSegments.length - 1];
                const testFile = new File([blob], name, { type: mimeType });
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(testFile);
                el.files = dataTransfer.files;
                el.dispatchEvent(new Event('change'));
                return subject;
            })
            .then(_ => cy.wait(1000));
    });
});

Cypress.Commands.add('login', (overrides = {}) => {
    Cypress.log({
      name: 'loginViaAuth0',
    });
  
    const options = {
      method: 'POST',
      url: 'dev-onyah706.auth0.com',
      body: {
        grant_type: 'password',
        username: 'wachirawit.th@ku.th',
        password: 'Note26za+',
        audience: 'http://hasura-dev.wefresh.me/v1/graphql',
        scope: 'openid profile email',
        client_id: '9kyS6tsGNY1Ry1hX6OTblCXQUTX8yq1H',
        client_secret: 'JEfi4jUHIH53MDGR9EH4YfNMtbfUS88jM-zsUPI7qqPDlbyPLuNw6UCxwVy-h4Fq',
      },
    };
    cy.request(options);
  });